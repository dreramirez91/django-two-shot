from django.forms import ModelForm
from .models import Receipt, ExpenseCategory, Account


class CreateReceipt(ModelForm):
    class Meta:
        model = Receipt
        fields = (
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        )


class CreateCategory(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = ("name",)


class CreateAccount(ModelForm):
    class Meta:
        model = Account
        fields = (
            "name",
            "number",
        )
