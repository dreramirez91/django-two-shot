from django.shortcuts import render, redirect
from .models import ExpenseCategory, Receipt, Account
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from .forms import CreateReceipt, CreateCategory, CreateAccount


@login_required
def show_receipts(request):
    user_receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"user_receipts": user_receipts}
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceipt(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")

    else:
        form = CreateReceipt()

    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories": categories}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {"accounts": accounts}
    return render(request, "receipts/account.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategory(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")

    else:
        form = CreateCategory()

    context = {"form": form}
    return render(request, "receipts/createcategory.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")

    else:
        form = CreateAccount()

    context = {"form": form}
    return render(request, "receipts/createaccount.html", context)
